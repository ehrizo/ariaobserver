﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using DataBase.ViewModel;
using DataBase.Controller;
using Microsoft.Web.WebSockets;

namespace AriaObserver.Manager
{
    public class Worker
    {
        public int Save(MutationViewModel e)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            string jsonData = js.Serialize(e);

            MutationController cMutation = new MutationController();
            MutationViewModel mutationVM = new MutationViewModel();
            mutationVM.Id = 0;
            mutationVM.ObserverId = 0;
            mutationVM.BundleId = 0;
            mutationVM.Name = e.Name;
            mutationVM.NodeType = e.NodeType;
            mutationVM.Height = e.Height;
            mutationVM.Width = e.Width;
            mutationVM.PositionTop = e.PositionTop;
            mutationVM.PositionLeft = e.PositionLeft;
            mutationVM.ParentId = e.ParentId;
            mutationVM.ParentType = e.ParentType;
            mutationVM.HtmlCode = e.HtmlCode;
            mutationVM.AriaAttributes = e.AriaAttributes;
            mutationVM.Attributes = e.Attributes;
            mutationVM.Json = jsonData;
            mutationVM.Url = e.Url;
            mutationVM.PngBase64 = e.PngBase64;
            mutationVM.childCount = e.childCount;
            mutationVM.children = e.children;
            mutationVM.domHeight = e.domHeight;
            mutationVM.domLevelMoreElements = e.domLevelMoreElements;
            mutationVM.domLevelElements = e.domLevelElements;
            mutationVM.domWidth = e.domWidth;
            mutationVM.childHTML = e.childHTML;
            mutationVM.mouseX = e.mouseX;
            mutationVM.mouseY = e.mouseY;
            mutationVM.eventType = e.eventType;
            mutationVM.elementSourceType = e.elementSourceType;
            mutationVM.elementSourceName = e.elementSourceName;
            mutationVM.numWords = e.numWords;
            mutationVM.numWordsChild = e.numWordsChild;
            mutationVM.textContent = e.textContent;
            mutationVM.textNodes = e.textNodes;
            mutationVM.textNodesCount = e.textNodesCount;
            mutationVM.sessionId = e.sessionId;
            mutationVM.numAHref = e.numAHref;
            mutationVM.numOnClick = e.numOnClick;
            mutationVM.clientIP = e.clientIP;
            mutationVM.clientName = e.clientName;
            mutationVM.SubElements = e.SubElements;

            return cMutation.Add(mutationVM);
        }
    }
}