﻿using DataBase.ViewModel;
using Microsoft.Web.WebSockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace AriaObserver.Manager
{
    public class ReceiverWebSocket : WebSocketHandler
    {
        private string _clientIP = string.Empty;
        private string _clientName = string.Empty;

        public ReceiverWebSocket(string clientIP, string clientName)
        {
            _clientIP = clientIP;
            _clientName = clientName;
        }

        public override void OnOpen()
        {
            base.Send("Connected to the server.");
            //base.OnOpen();
        }

        public override void OnMessage(string message)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                MutationViewModel e = js.Deserialize<MutationViewModel>(message);
                e.numWordsChild = e.textNodesCount > 0 ? (double)e.numWords / e.textNodesCount : 0;
                e.clientIP = _clientIP;
                e.clientName = _clientName;

                Manager.Worker wk = new Manager.Worker();
                wk.Save(e);
            }
            catch (Exception ex)
            {
                base.Send(ex.Message);
            }
//            base.OnMessage(message);
        }

        public override void OnClose()
        {
            base.OnClose();
        }

        public override void OnError()
        {
            base.OnError();
        }
    }
}