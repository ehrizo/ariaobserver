﻿using System.Threading.Tasks;
using System.Web.Mvc;
using DataBase.ViewModel;
using AriaObserver.Filters;

namespace AriaObserver.Controllers
{
    public class ReceiverController : AsyncController
    {
        [EnableCORSFilter]
        [HttpPost]
        [ValidateInput(false)]
        public void PushAsync(int Id, string Name, int PositionLeft, int PositionTop,
                         int Width, int Height, string HtmlCode, string NodeType,
                         string ParentId, string ParentType, string Attributes, 
                         int AriaAttributes, string Url, string PngBase64, int ChildCount,
                         string Children, int DomHeight, int DomLevelMoreElements, int DomLevelElements,
                         int DomWidth, string ChildHTML, int MouseX, int MouseY,
                         string EventType, string ElementSourceType, string ElementSourceName,
                         int NumWords, string TextContent, string TextNodes, int TextNodesCount, string SessionId, 
                         int NumAHref, int NumOnClick)
        {
            AsyncManager.OutstandingOperations.Increment();

            MutationViewModel e = new MutationViewModel();
            e.Id = Id;
            e.Name = Name;
            e.PositionLeft = PositionLeft;
            e.PositionTop = PositionTop;
            e.Width = Width;
            e.Height = Height;
            e.HtmlCode = HtmlCode;
            e.NodeType = NodeType;
            e.ParentId = ParentId;
            e.ParentType = ParentType;
            e.Attributes = Attributes;
            e.AriaAttributes = AriaAttributes;
            e.Url = Url;
            e.PngBase64 = PngBase64;
            e.childCount = ChildCount;
            e.children = Children;
            e.domHeight = DomHeight;
            e.domLevelMoreElements = DomLevelMoreElements;
            e.domLevelElements = DomLevelElements;
            e.domWidth = DomWidth;
            e.childHTML = ChildHTML;
            e.mouseX = MouseX;
            e.mouseY = MouseY;
            e.eventType = EventType;
            e.elementSourceType = ElementSourceType;
            e.elementSourceName = ElementSourceName;
            e.numWords = NumWords;
            e.numWordsChild = TextNodesCount > 0 ? (double)NumWords / TextNodesCount : 0;
            e.textContent = TextContent;
            e.textNodes = TextNodes;
            e.textNodesCount = TextNodesCount;
            e.sessionId = SessionId;
            e.numAHref = NumAHref;
            e.numOnClick = NumOnClick;
            e.clientIP = Request.UserHostAddress;
            e.clientName = Request.UserHostName;

            Manager.Worker wk = new Manager.Worker();
            Task.Factory.StartNew(() =>
            {
                string elementId = "";
                int id = wk.Save(e);
                AsyncManager.Parameters["id"] = id;
                AsyncManager.Parameters["elementId"] = elementId;
                AsyncManager.OutstandingOperations.Decrement();
            });
        }

        //public ActionResult PushCompleted(bool response)
        //{
        //    return View()
        //}
        public void PushCompleted(int id, string elementId)
        {
            if (id > 0)
            {
                //Nothing to do...
            }
            else
            {
                //Do something...
            }

        }
    }
}