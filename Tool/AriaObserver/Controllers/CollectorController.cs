﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AriaObserver.Controllers
{
    public class CollectorController : Controller
    {
        public JavaScriptResult CollectorScript(int observerId = 0, int bundleId = 0)
        {
            ViewBag.ObserverId = observerId;
            ViewBag.BundleId = bundleId;

            var script = string.Format(@"
               function getImage(element){{
                  var pngBase64 = '';
                  html2canvas(element, {{
                    onrendered: function (canvas) {{
                        pngBase64 = canvas.toDataURL();
                        //document.body.appendChild(canvas);
                    }}
                  }});
                  return pngBase64;
                }}
   
                $(document).ready(function () {{
                    var target = document.getElementById('testList');
                    var config = {{
                        childList: true,
                        subtree: true,
                        attributes: true,
                        characterData: false,
                        attributeOldValue: false,
                        characterDataOldValue: false
                       }};
                    //note this observe method
                    observer.observe(document.body, config);
                    console.log('registered');
                    }});

                    var observer = new MutationObserver(function (mutationRecords, observer) {{
                    mutationRecords.forEach(function (mutation) {{
                        /* Only fot tests -------------------------------------------------
                        console.log('type: ', mutation.type);
                        console.log('target: ',mutation.target);
                        console.log('addedNodes: ',mutation.addedNodes);
                        console.log('removedNodes: ',mutation.removedNodes);
                        console.log('previousSibling: ',mutation.previousSibling);
                        console.log('nextSibling: ',mutation.nextSibling);
                        console.log('attributeName: ',mutation.attributeName);
                        console.log('attributeNamespace: ',mutation.attributeNamespace);
                        console.log('oldValue: ',mutation.oldValue);
                        -------------------------------------------------------------------- */

                        if (mutation.target.nodeName != 'BODY' && mutation.target.nodeName != 'HTML') {{
                            var attributes = '';
                            var ariaAttributes = 0;
                            for (var i = 0; i < mutation.target.attributes.length; i++) {{
                                attributes += mutation.target.attributes[i].name + '=""' + mutation.target.attributes[i].textContent + '"" ';
                                if (ariaAttributes == 0) {{
                                    if (mutation.target.attributes[i].name.includes('aria-')) {{
                                        ariaAttributes = 1;
                                    }}
                                }}
                            }}

                            var pngBase64 = '';
                            //pngBase64 = GetImage(mutation.target);
                            
                            var children = '';
                            var childrenArray = Array.prototype.slice.call(mutation.target.children);
                            for (var i = 0; i < childrenArray.length; i++) {{
                                children += childrenArray[i].nodeName;
                                if (i+1 < childrenArray.length) {{
                                    children += ',';
                                }}
                            }}
                        
                            var mutationData = {{
                                Id: 0,
                                Name: mutation.target.id,
                                PositionLeft: mutation.target.offsetLeft,
                                PositionTop: mutation.target.offsetTop,
                                Width: mutation.target.clientWidth,
                                Height: mutation.target.clientHeight,
                                HtmlCode: mutation.target.outerHTML,
                                NodeType: mutation.target.nodeName,
                                ParentId: mutation.target.parentElement.id,
                                ParentType: mutation.target.parentElement.nodeName,
                                Attributes: attributes,
                                AriaAttributes: ariaAttributes,
                                Url: window.location.href,
                                PngBase64: pngBase64,
                                ChildCount: mutation.target.childElementCount,
                                Children: children,
                                ChildHTML: mutation.target.innerHTML,
                                MouseX: event.clientX,
                                MouseY: event.clientY,
                                EventType: event.type,
                                ElementSourceType: event.target.tagName,
                                ElementSourceName: event.target.id
                            }};

                            $.ajax({{
                                type: 'POST',
                                url: 'http://ehrizo.gear.host/ariaobserver/Receiver/Push',
                                data: mutationData,
                                success: function(r) {{
                                    //do something....
                                }}
                            }});
                        }}
                    }});
                 }})");

            return JavaScript(script);
        }
    }
}