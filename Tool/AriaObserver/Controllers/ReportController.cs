﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using db = DataBase.Controller;
using vm = DataBase.ViewModel;

namespace AriaObserver.Controllers
{
    public class ReportController : Controller
    {
        private void FillViewBags()
        {
            db.MutationController ctlMutation = new db.MutationController();
            ViewBag.InteractionLog = new SelectList(ctlMutation.getUserIds());

            db.ClassController ctlClass = new db.ClassController();
            ViewBag.Class = ctlClass.GetAll();
        }
        // GET: Report
        public ActionResult CapturedImages()
        {
            FillViewBags();
            return View();
        }

        [HttpPost]
        public ActionResult CapturedImages(string ddlInteraction, string ckbShowImage = "1")
        {
            ViewBag.ShowImage = ckbShowImage;
            db.MutationController ctlMutation = new db.MutationController();
            var selectionData = ddlInteraction.Split('-');
            var data = ctlMutation.getMutations(selectionData[2].Trim());
            FillViewBags();
            return View(data);
        }

        public ActionResult Mutations()
        {
            FillViewBags();
            return View();
        }

        [HttpPost]
        public ActionResult Mutations(string interaction)
        {
            db.MutationController ctlMutation = new db.MutationController();
            var selectionData = interaction.Split('-');
            var data = ctlMutation.getMutations(selectionData[2].Trim());
            FillViewBags();
            return View(data);
        }

        [HttpPost]
        public int SetClass(int mutationId, int classId)
        {
            return new db.MutationController().SetClass(mutationId, classId); 
        }

        public ActionResult GetChildren(int id)
        {
            db.ChildrenController ctlChildren = new db.ChildrenController();
            var data = ctlChildren.Get(id);
            if (data != null)
                return Json(data, JsonRequestBehavior.AllowGet);
            else
                return Json(new object[] { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public int SetChildrenClass(int childrenId, string isSubElement)
        {
            return new db.ChildrenController().SetClass(childrenId, isSubElement);
        }

        public string GetClassName(int classId)
        {
            return new db.ClassController().getName(classId);
        }
    }
}