﻿using AriaObserver.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.WebSockets;

namespace AriaObserver.Controllers
{
    public class WebSocketHandlerController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Ws()
        {
            if (HttpContext.Current.IsWebSocketRequest || HttpContext.Current.IsWebSocketRequestUpgrading)
            {
                HttpContext.Current.AcceptWebSocketRequest(ProcessWebsocketSession);
            }
            return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
        }

        private Task ProcessWebsocketSession(AspNetWebSocketContext context)
        {
            //string id = context.QueryString["id"];
            string clientIP = context.UserHostAddress;
            string clientName = context.UserHostName;

            var handler = new ReceiverWebSocket(clientIP, clientName);
            var processTask = handler.ProcessWebSocketRequestAsync(context);
            return processTask;
        }
    }
}