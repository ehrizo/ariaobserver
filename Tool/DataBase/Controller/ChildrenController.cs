﻿using DataBase.Model;
using DataBase.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.Controller
{
    public class ChildrenController
    {
        public int Add(ChildrenViewModel children)
        {
            Children c = new Children()
            {
                Id = 0,
                MutationId = children.MutationId,
                NodeType = children.NodeType,
                ParentType = children.ParentType,
                Attributes = children.Attributes,
                AriaAttributes = children.AriaAttributes,
                ChildCount = children.ChildCount,
                Childrens = children.Childrens,
                PositionLeft = children.PositionLeft,
                PositionTop = children.PositionTop,
                Width = children.Width,
                Height = children.Height,
                TextNodeCount = children.TextNodeCount,
                DomHeight = children.DomHeight,
                DomLevelMoreElements = children.DomLevelMoreElements,
                DomLevelElements = children.DomLevelElements,
                DomWidth = children.DomWidth,
                AvChildWidth = children.AvChildWidth,
                AvChildHeight = children.AvChildHeight,
                SdChildWidth = children.SdChildWidth,
                SdChildHeight = children.SdChildHeight,
                AvChildX = children.AvChildX,
                AvChildY = children.AvChildY,
                SdChildX = children.SdChildX,
                SdChildY = children.SdChildY,
                HtmlCode = children.HtmlCode,
                SubElement = children.SubElement
            };
            return c.Add();
        }

        public List<ChildrenViewModel> Get(int mutationId)
        {
            List<Children> data = new Children().Get(mutationId);
            List<ChildrenViewModel> dataVM = null;
            if (data != null && data.Count > 0)
            {
                dataVM = (from d in data
                          select new ChildrenViewModel()
                          {
                              Id = d.Id,
                              MutationId = d.MutationId,
                              NodeType = d.NodeType,
                              ParentType = d.ParentType,
                              Attributes = d.Attributes,
                              AriaAttributes = d.AriaAttributes,
                              ChildCount = d.ChildCount,
                              Childrens = d.Childrens,
                              PositionLeft = d.PositionLeft,
                              PositionTop = d.PositionTop,
                              Width = d.Width,
                              Height = d.Height,
                              TextNodeCount = d.TextNodeCount,
                              DomHeight = d.DomHeight,
                              DomLevelMoreElements = d.DomLevelMoreElements,
                              DomLevelElements = d.DomLevelElements,
                              DomWidth = d.DomWidth,
                              AvChildWidth = d.AvChildWidth,
                              AvChildHeight = d.AvChildHeight,
                              SdChildWidth = d.SdChildWidth,
                              SdChildHeight = d.SdChildHeight,
                              AvChildX = d.AvChildX,
                              AvChildY = d.AvChildY,
                              SdChildX = d.SdChildX,
                              SdChildY = d.SdChildY,
                              HtmlCode = d.HtmlCode,
                              SubElement = d.SubElement
                          }).ToList();
                return dataVM;
            }
            else
                return null;
        }

        public int SetClass(int childrenId, string isSubElement)
        {
            return new Children().SetClass(childrenId, isSubElement); 
        }
    }
}
