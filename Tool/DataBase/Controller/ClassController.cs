﻿using DataBase.Model;
using DataBase.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.Controller
{
    public class ClassController
    {
        public List<ClassViewModel> GetAll()
        {
            var data = new Class().GetAll();
            List<ClassViewModel> dataVM = null;
            if (data != null && data.Count > 0)
            {
                dataVM = (from d in data
                          select new ClassViewModel()
                          {
                              Id = d.Id,
                              Name = d.Name
                          }).ToList();
            }
            return dataVM;
        }

        public string getName(int id)
        {
            return new Class().getName(id);
        }

    }
}
