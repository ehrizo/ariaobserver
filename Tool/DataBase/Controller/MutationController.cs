﻿using DataBase.Model;
using DataBase.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.Controller
{
    public class MutationController
    {
        public MutationController()
        {

        }

        public int Add(MutationViewModel mutation)
        {
            Mutation m = new Mutation()
            {
                Id = 0,
                ObserverId = mutation.ObserverId,
                BundleId = mutation.BundleId,
                AriaAttributes = mutation.AriaAttributes,
                Attributes = mutation.Attributes,
                Height = mutation.Height,
                HtmlCode = mutation.HtmlCode,
                Name = mutation.Name,
                NodeType = mutation.NodeType,
                ParentId = mutation.ParentId,
                ParentType = mutation.ParentType,
                PositionLeft = mutation.PositionLeft,
                PositionTop = mutation.PositionTop,
                Width = mutation.Width,
                Json = mutation.Json,
                Url = mutation.Url,
                PngBase64 = mutation.PngBase64,
                childCount = mutation.childCount,
                children = mutation.children,
                domHeight = mutation.domHeight,
                domLevelMoreElements = mutation.domLevelMoreElements,
                domLevelElements = mutation.domLevelElements,
                domWidth = mutation.domWidth,
                childHTML = mutation.childHTML,
                mouseX = mutation.mouseX,
                mouseY = mutation.mouseY,
                eventType = mutation.eventType,
                elementSourceType = mutation.elementSourceType,
                elementSourceName = mutation.elementSourceName,
                numWords = mutation.numWords,
                numWordsChild = mutation.numWordsChild,
                textContent = mutation.textContent,
                textNodes = mutation.textNodes,
                textNodesCount = mutation.textNodesCount,
                sessionId = mutation.sessionId,
                numAHref = mutation.numAHref,
                numOnClick = mutation.numOnClick,
                clientIP = mutation.clientIP,
                clientName = mutation.clientName
            };

            int retorno = m.Add();

            if (retorno > 0)
            {
                ChildrenController ctlChildren = new ChildrenController();
                if (mutation.SubElements != null && mutation.SubElements.Count > 0)
                {
                    foreach (ChildrenViewModel cvm in mutation.SubElements)
                    {
                        cvm.MutationId = retorno;
                        ctlChildren.Add(cvm);
                    }
                }
                //retorno = ctlChildren.Add(mutation.SubElements);
            }

            return retorno;
        }

        public List<string> getUserIds()
        {
            return new Mutation().getUserIds();
        }

        public List<MutationViewModel> getMutations(string userId)
        {
            List<Mutation> data = new Mutation().getMutations(userId);
            List<MutationViewModel> dataVM = null;
            if (data != null)
            {
                dataVM = (from d in data
                          select new MutationViewModel()
                          {
                              Id = d.Id,
                              ClassId = d.ClassId,
                              ObserverId = d.ObserverId,
                              BundleId = d.BundleId,
                              Name = d.Name,
                              PositionLeft = d.PositionLeft,
                              PositionTop = d.PositionTop,
                              Width = d.Width,
                              Height = d.Height,
                              HtmlCode = d.HtmlCode,
                              NodeType = d.NodeType,
                              ParentId = d.ParentId,
                              ParentType = d.ParentType,
                              Attributes = d.Attributes,
                              AriaAttributes = d.AriaAttributes,
                              Json = d.Json,
                              Url = d.Url,
                              PngBase64 = d.PngBase64,
                              childCount = d.childCount,
                              children = d.children,
                              domHeight = d.domHeight,
                              domLevelMoreElements = d.domLevelMoreElements,
                              domLevelElements = d.domLevelElements,
                              domWidth = d.domWidth,
                              childHTML = d.childHTML,
                              mouseX = d.mouseX,
                              mouseY = d.mouseY,
                              eventType = d.eventType,
                              elementSourceType = d.elementSourceType,
                              elementSourceName = d.elementSourceName,
                              numWords = d.numWords,
                              numWordsChild = d.numWordsChild,
                              textContent = d.textContent,
                              textNodes = d.textNodes,
                              textNodesCount = d.textNodesCount,
                              mutationDate = d.mutationDate,
                              sessionId = d.sessionId,
                              numAHref = d.numAHref,
                              numOnClick = d.numOnClick,
                              clientIP = d.clientIP,
                              clientName = d.clientName
                          }).ToList();
            }
            return dataVM;
        }

        public int SetClass(int mutationId, int classId)
        {
            return new Mutation().SetClass(mutationId, classId);
        }
    }
}
