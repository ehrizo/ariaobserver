﻿using DataBase.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.DAL
{
    internal class ChildrenDAO : Banco
    {
        #region MÉTODOS PARA CONVERTER DATATABLE EM OBJETOS
        private Children ConvertToObject(DataRow row)
        {
            Children c = new Children()
            {
                Id = int.Parse(row["id"].ToString()),
                MutationId = int.Parse(row["mutationId"].ToString()),
                NodeType = row["nodeType"].ToString(),
                ParentType = row["parentType"].ToString(),
                Attributes = row["attributes"].ToString(),
                AriaAttributes = int.Parse(row["ariaAttributes"].ToString()),
                ChildCount = int.Parse(row["childCount"].ToString()),
                Childrens = row["children"].ToString(),
                PositionLeft = int.Parse(row["positionLeft"].ToString()),
                PositionTop = int.Parse(row["positionTop"].ToString()),
                Width = int.Parse(row["width"].ToString()),
                Height = int.Parse(row["height"].ToString()),
                TextNodeCount = int.Parse(row["textNodeCount"].ToString()),
                DomHeight = int.Parse(row["domHeight"].ToString()),
                DomLevelMoreElements = int.Parse(row["domLevelMoreElements"].ToString()),
                DomLevelElements = int.Parse(row["domLevelElements"].ToString()),
                DomWidth = int.Parse(row["domWidth"].ToString()),
                AvChildWidth = double.Parse(row["avChildWidth"].ToString()),
                AvChildHeight = double.Parse(row["avChildHeight"].ToString()),
                SdChildWidth = double.Parse(row["sdChildWidth"].ToString()),
                SdChildHeight = double.Parse(row["sdChildHeight"].ToString()),
                AvChildX = double.Parse(row["avChildX"].ToString()),
                AvChildY = double.Parse(row["avChildY"].ToString()),
                SdChildX = double.Parse(row["sdChildX"].ToString()),
                SdChildY = double.Parse(row["sdChildY"].ToString()),
                HtmlCode = row["htmlCode"].ToString(),
                SubElement = row["subElement"].ToString()
            };
            return c;
        }

        private List<Children> ConvertToList(DataTable dt)
        {
            List<Children> dados = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                dados = new List<Children>();
                foreach (DataRow dr in dt.Rows)
                    dados.Add(ConvertToObject(dr));
            }
            return dados;
        }
        #endregion

        internal int Add(Children children)
        {
            ComandoSQL.Parameters.Clear();
            ComandoSQL.CommandText = @"insert into Children (mutationId, nodeType, parentType, attributes, ariaAttributes, childCount,
                                                children, positionLeft, positionTop, width, height, textNodeCount, domHeight,
                                                domLevelMoreElements, domLevelElements, domWidth, avChildWidth, avChildHeight,
                                                sdChildWidth, sdChildHeight, avChildX, avChildY, sdChildX, sdChildY, htmlCode, subElement) 
                                       values (@mutationId, @nodeType, @parentType, @attributes, @ariaAttributes, @childCount,
                                               @children, @positionLeft, @positionTop, @width, @height, @textNodeCount, @domHeight,
                                               @domLevelMoreElements, @domLevelElements, @domWidth, @avChildWidth, @avChildHeight,
                                               @sdChildWidth, @sdChildHeight, @avChildX, @avChildY, @sdChildX, @sdChildY, @htmlCode, @subElement)";
            ComandoSQL.Parameters.AddWithValue("@mutationId", children.MutationId);
            ComandoSQL.Parameters.AddWithValue("@nodeType", children.NodeType);
            ComandoSQL.Parameters.AddWithValue("@parentType", children.ParentType);
            ComandoSQL.Parameters.AddWithValue("@attributes", children.Attributes);
            ComandoSQL.Parameters.AddWithValue("@ariaAttributes", children.AriaAttributes);
            ComandoSQL.Parameters.AddWithValue("@childCount", children.ChildCount);
            ComandoSQL.Parameters.AddWithValue("@children", children.Childrens);
            ComandoSQL.Parameters.AddWithValue("@positionLeft", children.PositionLeft);
            ComandoSQL.Parameters.AddWithValue("@positionTop", children.PositionTop);
            ComandoSQL.Parameters.AddWithValue("@width", children.Width);
            ComandoSQL.Parameters.AddWithValue("@height", children.Height);
            ComandoSQL.Parameters.AddWithValue("@textNodeCount", children.TextNodeCount);
            ComandoSQL.Parameters.AddWithValue("@domHeight", children.DomHeight);
            ComandoSQL.Parameters.AddWithValue("@domLevelMoreElements", children.DomLevelMoreElements);
            ComandoSQL.Parameters.AddWithValue("@domLevelElements", children.DomLevelElements);
            ComandoSQL.Parameters.AddWithValue("@domWidth", children.DomWidth);
            ComandoSQL.Parameters.AddWithValue("@avChildWidth", children.AvChildWidth);
            ComandoSQL.Parameters.AddWithValue("@avChildHeight", children.AvChildHeight);
            ComandoSQL.Parameters.AddWithValue("@sdChildWidth", children.SdChildWidth);
            ComandoSQL.Parameters.AddWithValue("@sdChildHeight", children.SdChildHeight);
            ComandoSQL.Parameters.AddWithValue("@avChildX", children.AvChildX);
            ComandoSQL.Parameters.AddWithValue("@avChildY", children.AvChildY);
            ComandoSQL.Parameters.AddWithValue("@sdChildX", children.SdChildX);
            ComandoSQL.Parameters.AddWithValue("@sdChildY", children.SdChildY);
            ComandoSQL.Parameters.AddWithValue("@htmlCode", children.HtmlCode);
            ComandoSQL.Parameters.AddWithValue("@subElement", children.SubElement);

            return ExecutaComando();
        }

        internal List<Children> Get(int mutationId)
        {
            ComandoSQL.Parameters.Clear();
            ComandoSQL.CommandText = @"select id, mutationId, nodeType, parentType, attributes, ariaAttributes, childCount,
                                              children, positionLeft, positionTop, width, height, textNodeCount, domHeight,
                                              domLevelMoreElements, domLevelElements, domWidth, avChildWidth, avChildHeight,
                                              sdChildWidth, sdChildHeight, avChildX, avChildY, sdChildX, sdChildY, htmlCode,
                                              subElement
                                       from Children
                                       where mutationId = @id";
            ComandoSQL.Parameters.AddWithValue("@id", mutationId);
            DataTable dt = ExecutaSelect();
            if (dt != null && dt.Rows.Count > 0)
                return ConvertToList(dt);
            else
                return null;
        }

        internal int SetClass(int childrenId, string isSubElement)
        {
            ComandoSQL.Parameters.Clear();
            ComandoSQL.CommandText = @"update Children set subElement = @isSubElement where id = @childrenId";
            ComandoSQL.Parameters.AddWithValue("@isSubElement", isSubElement);
            ComandoSQL.Parameters.AddWithValue("@childrenId", childrenId);
            return ExecutaComando();
        }
    }
}
