﻿using DataBase.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.DAL
{
    internal class MutationDAO : Banco
    {
        #region MÉTODOS PARA CONVERTER DATATABLE EM OBJETOS
        private Mutation ConvertToObject(DataRow row)
        {
            Mutation mut = new Mutation()
            {
                Id = int.Parse(row["id"].ToString()),
                ClassId = int.Parse(row["classId"].ToString()),
                ObserverId = int.Parse(row["observerId"].ToString()),
                BundleId = int.Parse(row["bundleId"].ToString()),
                Name = row["name"].ToString(),
                PositionLeft = int.Parse(row["positionLeft"].ToString()),
                PositionTop = int.Parse(row["positionTop"].ToString()),
                Width = int.Parse(row["width"].ToString()),
                Height = int.Parse(row["height"].ToString()),
                HtmlCode = row["htmlCode"].ToString(),
                NodeType = row["nodeType"].ToString(),
                ParentId = row["parentId"].ToString(),
                ParentType = row["parentType"].ToString(),
                Attributes = row["attributes"].ToString(),
                AriaAttributes = int.Parse(row["ariaattributes"].ToString()),
                Json = row["json"].ToString(),
                Url = row["url"].ToString(),
                PngBase64 = row["pngBase64"].ToString(),
                childCount = int.Parse(row["childCount"].ToString()),
                children = row["children"].ToString(),
                domHeight = int.Parse(row["domHeight"].ToString()),
                domLevelMoreElements = int.Parse(row["domLevelMoreElements"].ToString()),
                domLevelElements = int.Parse(row["domLevelElements"].ToString()),
                domWidth = int.Parse(row["domWidth"].ToString()),
                childHTML = row["childHTML"].ToString(),
                mouseX = int.Parse(row["mouseX"].ToString()),
                mouseY = int.Parse(row["mouseY"].ToString()),
                eventType = row["eventType"].ToString(),
                elementSourceType = row["elementSourceType"].ToString(),
                elementSourceName = row["elementSourceName"].ToString(),
                numWords = int.Parse(row["numWords"].ToString()),
                numWordsChild = double.Parse(row["numWordsChild"].ToString()),
                textContent = row["textContent"].ToString(),
                textNodes = row["textNodes"].ToString(),
                textNodesCount = int.Parse(row["textNodesCount"].ToString()),
                mutationDate = Convert.ToDateTime(row["mutationDate"].ToString()),
                sessionId = row["sessionId"].ToString(),
                numAHref = int.Parse(row["numAHref"].ToString()),
                numOnClick = int.Parse(row["numOnClick"].ToString()),
                clientIP = row["clientIP"].ToString(),
                clientName = row["clientName"].ToString()
            };
            return mut;
        }

        private List<Mutation> ConvertToList(DataTable dt)
        {
            List<Mutation> dados = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                dados = new List<Mutation>();
                foreach (DataRow dr in dt.Rows)
                    dados.Add(ConvertToObject(dr));
            }
            return dados;
        }
        #endregion

        internal int Add(Mutation mutation)
        {
            ComandoSQL.Parameters.Clear();
            ComandoSQL.CommandText = @"insert into MUTATION (observerId, bundleId, name, positionLeft, positionTop, 
                                       width, height, htmlCode, nodeType, parentId, parentType, attributes, ariaAttributes, 
                                       json, url, pngBase64, childCount, children, domHeight, domLevelMoreElements, domLevelElements,
                                       domWidth, childHTML, mouseX, mouseY, eventType, 
                                       elementSourceType, elementSourceName, numWords, numWordsChild, textContent, 
                                       textNodes, textNodesCount, mutationDate, sessionId, numAHref, numOnClick, clientIP, clientName)
                                       values (@observerId, @bundleId, @name, @positionLeft, @positionTop, 
                                       @width, @height, @htmlCode, @nodeType, @parentId, @parentType, @attributes, @ariaAttributes, 
                                       @json, @url, @pngBase64, @childCount, @children, @domHeight, @domLevelMoreElements, @domLevelElements,
                                       @domWidth, @childHTML, @mouseX, @mouseY, @eventType,
                                       @elementSourceType, @elementSourceName, @numWords, @numWordsChild, @textContent, 
                                       @textNodes, @textNodesCount, getdate(), @sessionId, @numAHref, @numOnClick, @clientIP, @clientName)";
            ComandoSQL.Parameters.AddWithValue("@observerId", mutation.ObserverId);
            ComandoSQL.Parameters.AddWithValue("@bundleId", mutation.BundleId);
            ComandoSQL.Parameters.AddWithValue("@name", mutation.Name);
            ComandoSQL.Parameters.AddWithValue("@positionLeft", mutation.PositionLeft);
            ComandoSQL.Parameters.AddWithValue("@positionTop", mutation.PositionTop);
            ComandoSQL.Parameters.AddWithValue("@width", mutation.Width);
            ComandoSQL.Parameters.AddWithValue("@height", mutation.Height);
            ComandoSQL.Parameters.AddWithValue("@htmlCode", mutation.HtmlCode);
            ComandoSQL.Parameters.AddWithValue("@nodeType", mutation.NodeType);
            ComandoSQL.Parameters.AddWithValue("@parentId", mutation.ParentId);
            ComandoSQL.Parameters.AddWithValue("@parentType", mutation.ParentType);
            ComandoSQL.Parameters.AddWithValue("@attributes", mutation.Attributes);
            ComandoSQL.Parameters.AddWithValue("@ariaAttributes", mutation.AriaAttributes);
            ComandoSQL.Parameters.AddWithValue("@json", mutation.Json);
            ComandoSQL.Parameters.AddWithValue("@url", mutation.Url);
            ComandoSQL.Parameters.AddWithValue("@pngBase64", mutation.PngBase64);
            ComandoSQL.Parameters.AddWithValue("@childCount", mutation.childCount);
            ComandoSQL.Parameters.AddWithValue("@children", mutation.children);
            ComandoSQL.Parameters.AddWithValue("@domHeight", mutation.domHeight);
            ComandoSQL.Parameters.AddWithValue("@domLevelMoreElements", mutation.domLevelMoreElements);
            ComandoSQL.Parameters.AddWithValue("@domLevelElements", mutation.domLevelElements);
            ComandoSQL.Parameters.AddWithValue("@domWidth", mutation.domWidth);
            ComandoSQL.Parameters.AddWithValue("@childHTML", mutation.childHTML);
            ComandoSQL.Parameters.AddWithValue("@mouseX", mutation.mouseX);
            ComandoSQL.Parameters.AddWithValue("@mouseY", mutation.mouseY);
            ComandoSQL.Parameters.AddWithValue("@eventType", mutation.eventType);
            ComandoSQL.Parameters.AddWithValue("@elementSourceType", mutation.elementSourceType);
            ComandoSQL.Parameters.AddWithValue("@elementSourceName", mutation.elementSourceName);
            ComandoSQL.Parameters.AddWithValue("@numWords", mutation.numWords);
            ComandoSQL.Parameters.AddWithValue("@numWordsChild", mutation.numWordsChild);
            ComandoSQL.Parameters.AddWithValue("@textContent", mutation.textContent);
            ComandoSQL.Parameters.AddWithValue("@textNodes", mutation.textNodes);
            ComandoSQL.Parameters.AddWithValue("@textNodesCount", mutation.textNodesCount);
            ComandoSQL.Parameters.AddWithValue("@sessionId", mutation.sessionId);
            ComandoSQL.Parameters.AddWithValue("@numAHref", mutation.numAHref);
            ComandoSQL.Parameters.AddWithValue("@numOnClick", mutation.numOnClick);
            ComandoSQL.Parameters.AddWithValue("@clientIP", mutation.clientIP);
            ComandoSQL.Parameters.AddWithValue("@clientName", mutation.clientName);

            int lastId = 0;
            ExecutaComando(false, out lastId);

            return lastId;
        }

        internal List<string> getUserIds()
        {
            List<string> ids = new List<string>();
            ComandoSQL.Parameters.Clear();
            ComandoSQL.CommandText = @"select distinct convert(varchar,mutationDate,103) as mutationDate, clientIP, sessionId from MUTATION order by 1 desc";
            DataTable dt = ExecutaSelect();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                    ids.Add(string.Format("{0} - {1} - {2}", Convert.ToDateTime(dt.Rows[i]["mutationDate"].ToString()).ToShortDateString(), dt.Rows[i]["clientIP"].ToString(), dt.Rows[i]["sessionId"].ToString()));
            }
            return ids;
        }

        internal List<Mutation> getMutations(string userId)
        {
            ComandoSQL.Parameters.Clear();
            ComandoSQL.CommandText = @"select * 
                                       from MUTATION where sessionId = @sessionId
                                       order by 1";
            ComandoSQL.Parameters.AddWithValue("@sessionId", userId);
            DataTable dt = ExecutaSelect();
            if (dt != null && dt.Rows.Count > 0)
                return ConvertToList(dt);
            else
                return null;
        }

        internal int SetClass(int mutationId, int classId)
        {
            ComandoSQL.Parameters.Clear();
            ComandoSQL.CommandText = @"update Mutation set classId = @classId where id = @id";
            ComandoSQL.Parameters.AddWithValue("@classId", classId);
            ComandoSQL.Parameters.AddWithValue("@id", mutationId);
            return ExecutaComando();
        }
    }
}
