﻿using DataBase.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.DAL
{
    internal class ClassDAO : Banco
    {
        #region MÉTODOS PARA CONVERTER DATATABLE EM OBJETOS
        private Class ConvertToObject(DataRow row)
        {
            Class cl = new Class()
            {
                Id = int.Parse(row["id"].ToString()),
                Name = row["name"].ToString()
            };
            return cl;
        }

        private List<Class> ConvertToList(DataTable dt)
        {
            List<Class> dados = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                dados = new List<Class>();
                foreach (DataRow dr in dt.Rows)
                    dados.Add(ConvertToObject(dr));
            }
            return dados;
        }
        #endregion

        internal List<Class> GetAll()
        {
            ComandoSQL.Parameters.Clear();
            ComandoSQL.CommandText = @"select * from Class";
            DataTable dt = ExecutaSelect();
            if (dt != null && dt.Rows.Count > 0)
                return ConvertToList(dt);
            else
                return null;
        }

        internal string getName(int id)
        {
            ComandoSQL.Parameters.Clear();
            ComandoSQL.CommandText = @"select name from Class where id = @id";
            ComandoSQL.Parameters.AddWithValue("@id", id);
            DataTable dt = ExecutaSelect();
            if (dt != null && dt.Rows.Count > 0)
                return dt.Rows[0]["name"].ToString();
            else
                return "Class not found.";
        }
    }
}
