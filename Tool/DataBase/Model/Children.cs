﻿using DataBase.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.Model
{
    internal class Children
    {
        internal int Id { get; set; }
        internal int MutationId { get; set; }
        internal string NodeType { get; set; }
        internal string ParentType { get; set; }
        internal string Attributes { get; set; }
        internal int AriaAttributes { get; set; }
        internal int ChildCount { get; set; }
        internal string Childrens { get; set; }
        internal int PositionLeft { get; set; }
        internal int PositionTop { get; set; }
        internal int Width { get; set; }
        internal int Height { get; set; }
        internal int TextNodeCount { get; set; }
        internal int DomHeight { get; set; }
        internal int DomLevelMoreElements { get; set; }
        internal int DomLevelElements { get; set; }
        internal int DomWidth { get; set; }
        internal double AvChildWidth { get; set; }
        internal double AvChildHeight { get; set; }
        internal double SdChildWidth { get; set; }
        internal double SdChildHeight { get; set; }
        internal double AvChildX { get; set; }
        internal double AvChildY { get; set; }
        internal double SdChildX { get; set; }
        internal double SdChildY { get; set; }
        internal string HtmlCode { get; set; }
        internal string SubElement { get; set; }

        internal int Add()
        {
            return new ChildrenDAO().Add(this);
        }

        internal List<Children> Get(int mutationId)
        {
            if (mutationId > 0)
                return new ChildrenDAO().Get(mutationId);
            else
                return null;
        }

        internal int SetClass(int childreId, string isSubElement)
        {
            if (childreId > 0)
                return new ChildrenDAO().SetClass(childreId, isSubElement);
            else
                return -10;
        }
    }
}
