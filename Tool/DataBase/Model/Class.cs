﻿using DataBase.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.Model
{
    internal class Class
    {
        private int _id;
        private string _name;

        internal int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        internal string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        internal List<Class> GetAll()
        {
            return new ClassDAO().GetAll();
        }

        internal string getName(int id)
        {
            return new ClassDAO().getName(id);
        }
    }
}
