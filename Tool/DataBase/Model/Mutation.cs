﻿using DataBase.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataBase.Model
{
    internal class Mutation
    {
        internal int Id { get; set; }
        internal int ClassId { get; set; }
        internal int ObserverId { get; set; }
        internal int BundleId { get; set; }
        internal string Name { get; set; }
        internal int PositionLeft { get; set; }
        internal int PositionTop { get; set; }
        internal int Width { get; set; }
        internal int Height { get; set; }
        internal string HtmlCode { get; set; }
        internal string NodeType { get; set; }
        internal string ParentId { get; set; }
        internal string ParentType { get; set; }
        internal string Attributes { get; set; }
        internal int AriaAttributes { get; set; }
        internal string Json { get; set; }
        internal string Url { get; set; }
        internal string PngBase64 { get; set; }
        internal int childCount { get; set; }
        internal string children { get; set; }
        internal int domHeight { get; set; }
        internal int domLevelMoreElements { get; set; }
        internal int domLevelElements { get; set; }
        internal int domWidth { get; set; }
        internal string childHTML { get; set; }
        internal int mouseX { get; set; }
        internal int mouseY { get; set; }
        internal string eventType { get; set; }
        internal string elementSourceType { get; set; }
        internal string elementSourceName { get; set; }
        internal int numWords { get; set; }
        internal double numWordsChild { get; set; }
        internal string textContent { get; set; }
        internal string textNodes { get; set; }
        internal int textNodesCount { get; set; }
        internal DateTime mutationDate { get; set; }
        internal string sessionId { get; set; }
        internal int numAHref { get; set; }
        internal int numOnClick { get; set; }
        internal string clientIP { get; set; }
        internal string clientName { get; set; }

        internal int Add()
        {
            return new MutationDAO().Add(this);
        }

        internal List<string> getUserIds()
        {
            return new MutationDAO().getUserIds();
        }

        internal List<Mutation> getMutations(string userId)
        {
            if (!string.IsNullOrEmpty(userId))
                return new MutationDAO().getMutations(userId);
            else
                return null;
        }

        internal int SetClass(int mutationId, int classId)
        {
            if (mutationId > 0 && classId > 0)
                return new MutationDAO().SetClass(mutationId, classId);
            else
                return -10;
        }
    }
}