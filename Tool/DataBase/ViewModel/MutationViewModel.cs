﻿using DataBase.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataBase.ViewModel
{
    public class MutationViewModel
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public int ObserverId { get; set; }
        public int BundleId { get; set; }
        public string Name { get; set; }
        public int PositionLeft { get; set; }
        public int PositionTop { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string HtmlCode { get; set; }
        public string NodeType { get; set; }
        public string ParentId { get; set; }
        public string ParentType { get; set; }
        public string Attributes { get; set; }
        public int AriaAttributes { get; set; }
        public string Json { get; set; }
        public string Url { get; set; }
        public string PngBase64 { get; set; }
        public int childCount { get; set; }
        public string children { get; set; }
        public int domHeight { get; set; }
        public int domLevelMoreElements { get; set; }
        public int domLevelElements { get; set; }
        public int domWidth { get; set; }
        public string childHTML { get; set; }
        public int mouseX { get; set; }
        public int mouseY { get; set; }
        public string eventType { get; set; }
        public string elementSourceType { get; set; }
        public string elementSourceName { get; set; }
        public int numWords { get; set; }
        public double numWordsChild { get; set; }
        public string textContent { get; set; }
        public string textNodes { get; set; }
        public int textNodesCount { get; set; }
        public DateTime mutationDate { get; set; }
        public string sessionId { get; set; }
        public int numAHref { get; set; }
        public int numOnClick { get; set; }
        public string clientIP { get; set; }
        public string clientName { get; set; }
        public List<ChildrenViewModel> SubElements { get; set; }
    }
}