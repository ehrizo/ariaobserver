﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.ViewModel
{
    public class ClassViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
