﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.ViewModel
{
    public class ChildrenViewModel
    {
        public int Id { get; set; }
        public int MutationId { get; set; }
        public string NodeType { get; set; }
        public string ParentType { get; set; }
        public string Attributes { get; set; }
        public int AriaAttributes { get; set; }
        public int ChildCount { get; set; }
        public string Childrens { get; set; }
        public int PositionLeft { get; set; }
        public int PositionTop { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int TextNodeCount { get; set; }
        public int DomHeight { get; set; }
        public int DomLevelMoreElements { get; set; }
        public int DomLevelElements { get; set; }
        public int DomWidth { get; set; }
        public double AvChildWidth { get; set; }
        public double AvChildHeight { get; set; }
        public double SdChildWidth { get; set; }
        public double SdChildHeight { get; set; }
        public double AvChildX { get; set; }
        public double AvChildY { get; set; }
        public double SdChildX { get; set; }
        public double SdChildY { get; set; }
        public string HtmlCode { get; set; }
        public string SubElement { get; set; }
    }
}
