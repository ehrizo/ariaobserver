/* ==============================================================================================
[INÍCIO]
   Função para obter um screenshot da aba ativa do navegador (retorno em base64)
================================================================================================*/
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
		chrome.tabs.captureVisibleTab(null,	{format: "png"}, function(dataUrl) {
			sendResponse({imgSrc:dataUrl});
		}); //remember that captureVisibleTab() is a statement
    return true;
	}
);
/* ==============================================================================================
[FIM]
================================================================================================*/