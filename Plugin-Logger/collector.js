/* ==============================================================================================
[INÍCIO]
   Função para obter a imagem do elemento que sofreu mutação (retorno em base64)
================================================================================================*/
/*
function getImage(mutation, posLeft, posTop, pixWidth, pixHeight){
	chrome.runtime.sendMessage({msg: "capture"}, function(response) {
		canvas = document.createElement("canvas");
		var partialImage = new Image();
		partialImage.onload = function() {
			var att = document.createAttribute("data-observer");
			att.value = "1";
			partialImage.setAttributeNode(att);

			canvas.width = pixWidth; //new image's width
			canvas.height = pixHeight; //new image's height
			var context = canvas.getContext("2d");
			//drawImage(new image, left position, top position, width, height, xCanvas, yCanvas, width, height)
			context.drawImage(partialImage,	posLeft, posTop, pixWidth, pixHeight, 0, 0, pixWidth, pixHeight);
			pngBase64 = canvas.toDataURL("image/png");
			//img.src = croppedDataUrl;
			//var divImages = document.getElementById("Images");
			//divImages.innerHTML = "<p><img src='" + pngBase64 + "' /></p>";

			mutation["PngBase64"] = pngBase64;
			//alert(JSON.stringify(mutation));
			
			//Chamada assincrona para persistência dos dados no BD
			var proto = "https://";
			if (location.protocol != "https")
				proto = "http://";
			
			$.ajax({
				type: 'POST',
				url: proto + 'www.unoeste.br/ariaobserver/Receiver/Push',
				data: mutation,
				success: function(r) {
					//pngBase64 = '';
					currentMutation = 'nothing';
				}
			});
			
		}	
		partialImage.src = response.imgSrc;
	});
}
*/
function getProtocol() {
	var proto = "https://";
	if (location.protocol != "https:")
		proto = "http://";	
	return proto;
}
 
function saveMutation(mutation, node, screenshot){
	if (screenshot) {
		var partialImage = new Image();
		partialImage.onload = function(){
			var att = document.createAttribute("data-observer");
			att.value = "1";
			partialImage.setAttributeNode(att);
			
			mutation["PngBase64"] = pngBase64;
				
			//Chamada assincrona para persistência dos dados no BD
			// $.ajax({
				// type: 'POST',
				// url: getProtocol() + 'www.unoeste.br/ariaobserver/Receiver/Push',
				// data: mutation,
				// success: function(r) {
					// currentMutation = 'nothing';
				// }
			// });	

			if (connection.readyState == WebSocket.OPEN) {
				connection.send(window.JSON.stringify(mutation));
			}
			else {
				console.error("Websocket is closed.");
			}
		}
		html2canvas(node, {
			onrendered: function (canvas) {
			pngBase64 = canvas.toDataURL();
			//document.body.appendChild(canvas);
			partialImage.src = pngBase64;
			}
		});
	}
	else {
		mutation["PngBase64"] = pngBase64;
		//Chamada assincrona para persistência dos dados no BD
		//$.ajax({
		//	type: 'POST',
		//	url: getProtocol() + 'www.unoeste.br/ariaobserver/Receiver/Push',
		//	data: mutation,
		//	success: function(r) {
		//		currentMutation = 'nothing';
		//	}
		//});
		
		//Envio dos dados via websocket
		if (connection.readyState == WebSocket.OPEN) {
			connection.send(window.JSON.stringify(mutation));
		}
		else {
			console.error("Websocket is closed.");
		}
	}
}
/* ==============================================================================================
[FIM]
=================================================================================================*/

/* ==============================================================================================
[INÍCIO]
   Função para obter o posicionamento dos elementos
================================================================================================*/   
function getPosition( element ) {
   var rect = element.getBoundingClientRect();
   return {x:Math.trunc(rect.left),y:Math.trunc(rect.top)};
}
/* ==============================================================================================
[FIM]
=================================================================================================*/
/* ==============================================================================================
[INÍCIO]
   Função para obter o tamanho dos elementos
================================================================================================*/   
function getSize( element ) {
   var rect = element.getBoundingClientRect();
   return {height:Math.trunc(rect.height),width:Math.trunc(rect.width)};
}
/* ==============================================================================================
[FIM]
=================================================================================================*/

/* ==============================================================================================
[INÍCIO]
   Função para checar a profundidade dos elementos dentro do elemento que sofreu a mutação
================================================================================================*/   
function getDomHeight(el,height,tagsHeight) {
	if (height > 0){
		tagsHeight.push(el.tagName+'|'+height);
		//console.log(el.tagName+'|'+domHeight);
		if (height > domHeight) {
			domHeight = height;
		}
	}
    for(var i=0; i<el.children.length; i++) {
        getDomHeight(el.children[i],height+1,tagsHeight,domHeight);
    }
	return tagsHeight.join(',');
}
/* ==============================================================================================
[FIM]
=================================================================================================*/

/* ==============================================================================================
[INÍCIO]
   Função para obter o nível da árvore com maior quantidade de elementos e quantos elementos ele possui
================================================================================================*/   
function getDomElementsByLevel(childrenHeight) {
	var workVector = childrenHeight.split(',');
	var dataVector = {};
	for (var i = 0; i < workVector.length; i++) {
		dataVector[workVector[i]] += workVector[i] + ',';
	}
	domLevelElements = 1;
	domLevelMoreElements = 1;
	for (key in dataVector) {
		var numElements = (dataVector[key].match(/,/g) || []).length;
		if (numElements > domLevelElements){
			domLevelElements = numElements;
			var l = key.split('|');
			if (l[1] > 1) {
				domLevelMoreElements = l[1] - 1;
			}
		}
	}
}
/* ==============================================================================================
[FIM]
=================================================================================================*/

/* ==============================================================================================
[INÍCIO]
   Função para retirar espaços, quebra de linha e deixar a string "limpa"
================================================================================================*/   
function cleanString(str) {
    str = str.replace(/(^\s*)|(\s*$)/gi,"");
	str = str.replace(/[ ]{2,}/gi," ");
	str = str.replace(/\n /,"\n");
	str = str.replace(/\r?\n|\r/g,"");
	//str = str.replace(/[^\u000A\u0020-\u007E]/g, ' ');
	return str;
}
/* ==============================================================================================
[FIM]
================================================================================================*/   

/* ==============================================================================================
[INÍCIO]
   Função para obter o número de palavras do elemento que sofreu mutação e dos seus filhos
================================================================================================*/   
function getNumWords(txt) {
    txt = cleanString(txt);
	var words = txt.split(' ');
	var count = 0;
	for (var i = 0; i < words.length; i++) {
		if (words[i] != '') {
			count++;
		}
	}
    return count;
}
/* ==============================================================================================
[FIM]
================================================================================================*/   

/* ==============================================================================================
[INÍCIO]
   Coleta os filhos do elemento que sofreu a mutação, a qtd de nós do tipo texto e o texto
================================================================================================*/   
function collectChildNodes(element, c, t) {
    for (var child= element.firstChild; child!==null; child= child.nextSibling) {
		if (child.tagName){
			c.push(child);
		}
        if (child.nodeType===3) {
			var txt = cleanString(child.data).trim();
			if (txt != '') {
				t.push(child);
			}
		}
        else if (child.nodeType===1)
            collectChildNodes(child, c, t);
    }
}
/* ==============================================================================================
[FIM]
================================================================================================*/

/* ==============================================================================================
[INÍCIO]
   Coleta os filhos dos filhos do elemento que sofreu a mutação, a qtd de nós do tipo texto e o texto
================================================================================================*/   
function collectChildChildNodes(element, c) {
    for (var child= element.firstChild; child!==null; child= child.nextSibling) {
		//if (child.hasChildNodes() && child.childNodes[0].nodeType != 3){
		if (child.hasChildNodes() && child.firstElementChild != null) {
			c.push(child);
		}
		if (child.nodeType===1)
            collectChildChildNodes(child, c);
    }
}
/* ==============================================================================================
[FIM]
================================================================================================*/

function getTextWithSpaces(t) {
    for (var i= t.length; i-->0;)
        t[i]= t[i].data;
    return t.join(' ');
}

/* ==============================================================================================
[INÍCIO]
   Função para calcular média, desvio padrão e variança
   http://jsfromhell.com/pt/array/average#rank-header
================================================================================================*/   
average = function(a){
    var r = {mean: 0, variance: 0, deviation: 0}, t = a.length;
    for(var m, s = 0, l = t; l--; s += a[l]);
    for(m = r.mean = s / t, l = t, s = 0; l--; s += Math.pow(a[l] - m, 2));
    return r.deviation = Math.sqrt(r.variance = s / t), r;
}
/* ==============================================================================================
[FIM]
================================================================================================*/

/* ==============================================================================================
[INÍCIO]
   Função para obter os atributos dos filhos da mutação
================================================================================================*/   
function getChildAttributes(c) {
	var childAttributes = '';
	for (var i = 0; i < c.attributes.length; i++) {
		childAttributes += c.attributes[i].name + '=' + c.attributes[i].textContent + ' ';
	}
	return childAttributes;	
}		
/* ==============================================================================================
[FIM]
================================================================================================*/


/* ==============================================================================================
[INÍCIO]
   Obtem a largura da árvore a partir dos nós folha e serializa os filhos da mutação com a sua profundidade
================================================================================================*/   
function getChildren(c) {
	var w = [];
	subElements = '[';
    //for (var i= c.length; i-->0;) {
	for (var i=0; i < c.length; i++) {
		if (!c[i].hasChildNodes()){
			w.push(c[i]);
		} else if (c[i].hasChildNodes() && c[i].childNodes[0].nodeType===3){
			if (!cleanString(c[i].childNodes[0].nodeValue).trim() == '') {
				w.push(c[i]);
			}
		}

		//if (c[i].hasChildNodes() && c[i].childNodes[0].nodeType != 3) {
		if (c[i].hasChildNodes() && c[i].firstElementChild != null) {
			//Obtem os elementos texto e os filhos de cada elemento
			var childTextNodes = [];
			var childElementsNodes = [];
			var childChildNodes = [];
			collectChildNodes(c[i], childElementsNodes, childTextNodes);
			collectChildChildNodes(c[i], childChildNodes);

			//Obtem os atributos vinculados a cada elemento filho e também verifica se ele possui atributos ARIA
			var childAttr = cleanString(getChildAttributes(c[i]));
			var childAriaAttributes = 0;
			if (childAttr.includes('aria-')) {
				childAriaAttributes = 1;
			}
			
			subElements = subElements + '{"MutationId": 0, ';
			subElements = subElements + '"NodeType": "' + c[i].nodeName + '", ';
			if (c[i].parentElement != null)
				subElements = subElements + '"ParentType": "' + c[i].parentElement.nodeName + '", ';
			else
				subElements = subElements + '"ParentType": "", ';
			
			// var positionLeft = c[i].offsetLeft || 0;
			// var positionTop = c[i].offsetTop || 0;
			var positionLeft = getPosition(c[i]).x;
			var positionTop = getPosition(c[i]).y;
			
			subElements = subElements + '"Attributes": "' + childAttr + '", ';
			subElements = subElements + '"AriaAttributes": ' + childAriaAttributes + ', ';
			subElements = subElements + '"ChildCount": ' + childElementsNodes.length + ', ';
			subElements = subElements + '"Childrens": "", ';
			subElements = subElements + '"PositionLeft": ' + positionLeft + ', ';
			subElements = subElements + '"PositionTop": ' + positionTop + ', ';
			//subElements = subElements + '"Width": ' + c[i].clientWidth + ', ';
			//subElements = subElements + '"Height": ' + c[i].clientHeight + ', ';
			subElements = subElements + '"Width": ' + getSize(c[i]).width + ', ';
			subElements = subElements + '"Height": ' + getSize(c[i]).height + ', ';
			subElements = subElements + '"TextNodeCount": ' + childTextNodes.length + ', ';
			subElements = subElements + '"DomHeight": 0, ';
			subElements = subElements + '"DomLevelMoreElements": 0, ';
			subElements = subElements + '"DomLevelElements": 0, ';
			subElements = subElements + '"DomWidth": 0, ';
			if (childChildNodes.length <= 0) {
				subElements = subElements + '"AvChildWidth": -1, ';
				subElements = subElements + '"AvChildHeight": -1, ';
				subElements = subElements + '"SdChildWidth": -1, ';
				subElements = subElements + '"SdChildHeight": -1, ';
				subElements = subElements + '"AvChildX": -1, ';
				subElements = subElements + '"AvChildY": -1, ';
				subElements = subElements + '"SdChildX": -1, ';
				subElements = subElements + '"SdChildY": -1, ';
			}
			else {
				//Vetores para calculo da média e desvio padrão da posição e do tamanho
				var arrayWidth = [];
				var arrayHeight = [];
				var arrayX = [];
				var arrayY = [];
				for (j = 0; j < childChildNodes.length; j++) {
					arrayWidth[j] = getSize(childChildNodes[j]).width;
					arrayHeight[j] = getSize(childChildNodes[j]).height;
					arrayX[j] = getPosition(childChildNodes[j]).x;
					arrayY[j] = getPosition(childChildNodes[j]).y;
				}
				var valuesWidth = average(arrayWidth);
				var valuesHeight = average(arrayHeight);
				var valuesPosX = average(arrayX);
				var valuesPosY = average(arrayY);
				subElements = subElements + '"AvChildWidth": ' + valuesWidth.mean + ', ';
				subElements = subElements + '"AvChildHeight": ' + valuesHeight.mean + ', ';
				subElements = subElements + '"SdChildWidth": ' + valuesWidth.deviation + ', ';
				subElements = subElements + '"SdChildHeight": ' + valuesHeight.deviation + ', ';
				subElements = subElements + '"AvChildX": ' + valuesPosX.mean + ', ';
				subElements = subElements + '"AvChildY": ' + valuesPosY.mean + ', ';
				subElements = subElements + '"SdChildX": ' + valuesPosX.deviation + ', ';
				subElements = subElements + '"SdChildY": ' + valuesPosY.deviation + ', ';
			}
			subElements = subElements + '"HtmlCode": "' + cleanString(c[i].outerHTML).replace(/[\\"]/g, "\'") + '", ';
			subElements = subElements + '"SubElement": "N"},';
		}
			
		if (c[i].nodeType===3) {
			c[i]= c[i].parentNode.tagName + '|' + i;
		}
		else {
			c[i]= c[i].tagName + '|' + i;
		}
	}
	domWidth = w.length;
	if (subElements.length > 1)
		subElements = subElements.substring(0,subElements.length-1);
	subElements = subElements + ']';
	subElements = $.parseJSON(subElements);
    return c.join(',');
}
/* ==============================================================================================
[FIM]
================================================================================================*/

/* ==============================================================================================
[INÍCIO]
   Serializa o conteúdo do vetor de nós texto para uma string
================================================================================================*/   
function getTextNodes(n) {
	var txt = [];
	for (var i = n.length; i-->0;){
		txt[i] = n[i].data;
		n[i] = n[i].parentNode.tagName;
	}
	return txt.join(' ');
}
/* ==============================================================================================
[FIM]
================================================================================================*/

/* ==============================================================================================
[INÍCIO]
   Verificar se houve eventos click e mouseover no documento para então capturar os seus valores
================================================================================================*/   
var mouseX = -1;
var mouseY = -1;
var eventType = "";
var sourceType = "";
var sourceName = "";
/*
mouseX = event.clientX;
mouseY = event.clientY;
eventType = event.type;
sourceType = event.target.tagName;
sourceName = event.target.id;
*/
document.onclick = function(e)
{
	mouseX = e.pageX;
	mouseY = e.pageY;
	eventType = "click";
	sourceType = e.target.nodeName;
	sourceName = e.target.id;
};
document.onmouseover = function(e)
{
	mouseX = e.pageX;
	mouseY = e.pageY;
	eventType = "mouseover";
	sourceType = e.target.nodeName;
	sourceName = e.target.id;
};
/* ==============================================================================================
[FIM]
================================================================================================*/   

/* ==============================================================================================
[INÍCIO]
   Inicialização da extensão
   Script para observar as mutações do corpo do documento e persistir no banco de dados
================================================================================================*/   
var pngBase64 = '';
var timeControl = 0;
var currentMutation = 'nothing';
var domHeight = 0;
var domLevelMoreElements = 0;
var domLevelElements = 0;
var domWidth = 0;
var connection;
var subElements = '';

$(document).ready(function () {
	//var target = document.getElementById('testList');
    var config = {
          childList: true,
          subtree: true,
          attributes: true,
          characterData: true,
          attributeOldValue: true,
          characterDataOldValue: true
    };
	//Cria uma sessionStorage para guardar um "ID" do usuário. Necessário pois o sessionId varia quando em requisições de diferentes domínios
	if (!sessionStorage.getItem("userId")){
		sessionStorage.setItem("userId",Date.now());
	}
	
    //Quando o documento estiver pronto, então o objeto observer já estará declarado
    observer.observe(document.body, config);
    console.log('ARIA Observer registered.');
	
	//Inicialização do websocket
	var proto = (window.location.protocol.indexOf("https") >= 0) ? "wss" : "ws";
    connection = new WebSocket(proto + "://www.unoeste.br/ariaobserver/api/WebSocketHandler/Ws");
	connection.onopen = function() {
		console.log("Websocket client connected.");
	};
	connection.onmessage = function(event) {
		console.log(event.data);
	}
});

var observer = new MutationObserver(function (mutationRecords, observer) {
	mutationRecords.forEach(function (mutation) {
    /* Only fot tests -------------------------------------------------
    console.log('type: ', mutation.type);
    console.log('target: ',mutation.target);
    console.log('addedNodes: ',mutation.addedNodes);
    console.log('removedNodes: ',mutation.removedNodes);
    console.log('previousSibling: ',mutation.previousSibling);
    console.log('nextSibling: ',mutation.nextSibling);
    console.log('attributeName: ',mutation.attributeName);
    console.log('attributeNamespace: ',mutation.attributeNamespace);
    console.log('oldValue: ',mutation.oldValue);
    -------------------------------------------------------------------- */

    if (mutation.target.nodeName != 'BODY' && mutation.target.nodeName != 'HTML') {
		currentMutation = mutation.target;

		//Verifica se há atributos ARIA na mutação
		//Também verifica se a mutação do elemento já foi registrada pelo collector
		var attributes = '';
		var wasRegistered = 0;
		var ariaAttributes = 0;
		for (var i = 0; i < currentMutation.attributes.length; i++) {
			attributes += currentMutation.attributes[i].name + '="' + currentMutation.attributes[i].textContent + '" ';
			if (wasRegistered == 0) {
				if (currentMutation.attributes[i].name.includes('data-observer')) {
					wasRegistered = 1;
				}				
				if (ariaAttributes == 0) {
					if (currentMutation.attributes[i].name.includes('aria-')) {
						ariaAttributes = 1;
					}
				}
			}
		}	

		//Registrar mutação no BD caso ainda não tenha ocorrido
		if (wasRegistered == 0) {
			var att = document.createAttribute("data-observer");
			att.value = "1";
			currentMutation.setAttributeNode(att);
			
			//Obter os nós do tipo texto e seus respectivos conteúdos
			var textNodes = [];
			var childNodes = [];
			collectChildNodes(currentMutation, childNodes, textNodes);
			//Por meio dos filhos da mutação obter os dados para armazenar na tabela children
			/*
			var subelements = [
				{
					MutationId: 154612,
					NodeType: "NT-Test",
					ParentType: "PT-Test",
					Attributes: "Att-Test",
					AriaAttributes: 1,
					ChildCount: 10,
					Childrens: "Chi-Test",
					PositionLeft: 10,
					PositionTop: 20,
					Width: 100,
					Height: 200,
					TextNodeCount: 10,
					DomHeight: 3,
					DomLevelMoreElements: 2,
					DomLevelElements: 4,
					DomWidth: 33
				},
				{
					MutationId: 154612,
					NodeType: "NT-Test2",
					ParentType: "PT-Test2",
					Attributes: "Att-Test2",
					AriaAttributes: 0,
					ChildCount: 20,
					Childrens: "Chi-Test2",
					PositionLeft: 20,
					PositionTop: 30,
					Width: 200,
					Height: 300,
					TextNodeCount: 20,
					DomHeight: 4,
					DomLevelMoreElements: 3,
					DomLevelElements: 5,
					DomWidth: 44
				}
			];
			//...
			*/
			var children = getChildren(childNodes);
			var textContent = getTextNodes(textNodes);
			var strTextNodes = textNodes.join(',');
			
			var tagsHeight = [];
			domHeight = 0;
			var childrenHeight = getDomHeight(currentMutation, 0, tagsHeight);
			getDomElementsByLevel(childrenHeight);
					
			//Dados da mutação a serem persistidos		
			var mutationData = {
				Id: 0,
				Name: currentMutation.id,
				PositionLeft: getPosition(currentMutation).x,
				PositionTop: getPosition(currentMutation).y,
				Width: getSize(currentMutation).width,
				Height: getSize(currentMutation).height,
				HtmlCode: cleanString(currentMutation.outerHTML),
				NodeType: currentMutation.nodeName,
				ParentId: currentMutation.parentElement != null ? currentMutation.parentElement.id : "",
				ParentType: currentMutation.parentElement != null ? currentMutation.parentElement.nodeName : "",
				Attributes: cleanString(attributes),
				AriaAttributes: ariaAttributes,
				Url: window.location.href,
				ChildCount: childNodes.length,
				//Children: children,
				Children: childrenHeight,
				DomHeight: domHeight,
				DomLevelMoreElements: domLevelMoreElements,
				DomLevelElements: domLevelElements,
				DomWidth: domWidth,
				ChildHTML: cleanString(currentMutation.innerHTML),
				MouseX: mouseX,
				MouseY: mouseY,
				EventType: eventType,
				ElementSourceType: sourceType,
				ElementSourceName: sourceName,
				NumWords: getNumWords(textContent),
				TextContent: textContent,
				TextNodes: strTextNodes,
				TextNodesCount: textNodes.length,
				SessionId: sessionStorage.getItem("userId"),
				CssElement: '',
				NumAHref: currentMutation.getElementsByTagName('a').length,
				NumOnClick: (currentMutation.innerHTML.toLowerCase().match(/onclick/g) || []).length,
				SubElements: subElements
			};

			/*
			setTimeout(function() { getImage(mutationData,
					 currentMutation.offsetLeft,
					 currentMutation.offsetTop,
					 currentMutation.clientWidth,
			currentMutation.clientHeight) },2000);
			*/
			//setTimeout(function() { getImage2(mutationData, currentMutation) },1500);
			saveMutation(mutationData, currentMutation, false);
			}
		}
    });
})
/* ==============================================================================================
[FIM]
================================================================================================*/